#!/bin/bash

sudo docker container stop mysql
sudo docker container rm mysql
sudo docker image rm mysql
cd sql/
sudo docker build -t mysql:latest .
sudo docker run -p 3306:3306 -d --name=mysql mysql:latest


sudo docker container stop backend
sudo docker container rm backend
sudo docker image rm backend
cd backend/
sudo docker build -t backend:latest .
sudo docker run -p 8000:8000 -d --name=backend --link mysql:mysql backend:latest

sudo docker container stop frontend
sudo docker container rm frontend
sudo docker image rm frontend
cd frontend/
sudo docker build -t frontend:latest .
sudo docker run -p 80:80 -d --name=frontend --link backend:backend  frontend:latest
